const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");
module.exports = {
    webpack: {
        plugins: {
            add: [
                new NodePolyfillPlugin()
            ],
        }
    },
    devServer: {
        headers: {
            'Cross-Origin-Embedder-Policy': 'require-corp',
            'Cross-Origin-Opener-Policy': 'same-origin'
        },
    },
};
