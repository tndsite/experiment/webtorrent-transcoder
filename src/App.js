import React from 'react';
import './App.css';
import VideoTranscoder from "./screen/VideoTranscoder";
import {createFFmpeg} from "@ffmpeg/ffmpeg";
//TODO Fix remote address for release
const ffmpeg = createFFmpeg({
  corePath: window.location.href + "ffmpeg-core.js",
  log: false,
});
function App() {
  return (
      <VideoTranscoder ffmpeg={ffmpeg}/>
  );
}

export default App;
