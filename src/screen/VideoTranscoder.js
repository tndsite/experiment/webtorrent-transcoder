import React, {Component} from 'react';
import {convertHMS, getDuration, load} from "../utils/FFMpegUtils";
import {
    Button,
    Chip,
    Container,
    createTheme,
    CssBaseline,
    Stack,
    TextField,
    ThemeProvider,
    Typography
} from "@mui/material";

import {FileUploadOutlined, PlayCircleOutlined} from "@mui/icons-material";
import WebTorrent from "webtorrent";
import Transcoder from "./Transcoder";


const defaultTheme = createTheme();
const options = {
    typography: {
        fontSize: 12,
    },
    palette: {
        mode: 'dark',
        background: {
            default: "#303030",
            paper: "#121212"
        }
    },
    components: {
        MuiLinearProgress: {
            styleOverrides: {
                root: {
                    borderRadius: "10px"
                }
            }
        },
        MuiListItemButton: {
            styleOverrides: {
                root: {
                    borderRadius: "20px",
                    paddingLeft: "10px",
                    paddingRight: "10px",
                }
            }
        },
        MuiContainer: {
            styleOverrides: {
                root: {
                    paddingLeft: "0px",
                    paddingRight: "0px",
                    height: "100%",
                    [defaultTheme.breakpoints.up('xs')]: {
                        paddingLeft: "0px",
                        paddingRight: "0px",
                        paddingTop: "5px",
                    }
                },
            },
        },
    },
};
let client = new WebTorrent();

const torrentId = "magnet:?xt=urn:btih:bf089fab221ff4ab033074797eda6c963beebc70&dn=sample.avi&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=wss%3A%2F%2Ftracker.quix.cf&tr=wss%3A%2F%2Ftracker.crawfish.cf"

class VideoTranscoder extends Component {
    state = {
        logs: [],
        parameter: "-s hd480 -crf 22 -c:a aac -b:a 160k -movflags frag_keyframe+empty_moov+default_base_moof -c:v libx264 -preset ultrafast",
        started: false,
        loading: false,
        ffmpegReady: false,
        theme: createTheme(options),
    }

    constructor() {
        super();
        this.videoElement = React.createRef();
    }

    log = (text, ...variable) => {
        // console.log(text, variable)
        this.setState((p) => {
            p.logs.push(text)
            return p;
        })
    }

    componentDidMount() {
        let {ffmpeg} = this.props;

        load(ffmpeg).then(
            () => {
                this.setState({
                    ffmpegReady: true
                })
            }
        )
        navigator.serviceWorker.register("sw.min.js", {scope: "/"})
            .then((reg) => {
                const worker = reg.active || reg.waiting || reg.installing
                let checkState = (worker) => {
                    return worker.state === 'activated' && client.loadWorker(worker, () => {
                        console.log("Service worker registered, scope: ", navigator.serviceWorker.controller);
                    })
                }
                if (!checkState(worker)) {
                    worker.addEventListener('statechange', ({target}) => checkState(target))
                }
            })
            .catch(function (error) {
                console.log("Service worker registration failed: " + error.message);
            });
    }

    transcode = async (file) => {
        let {ffmpeg} = this.props;
        let {parameter} = this.state;
        this.setState({loading: true, progress: 0})
        try {
            console.log('Loading ffmpeg-core.js', ffmpeg);
            await load(ffmpeg);
            let progress = 0;
            let myMediaSource = new MediaSource();
            this.videoElement.current.src = URL.createObjectURL(myMediaSource);
            let videoSourceBuffer;
            myMediaSource.addEventListener('sourceopen', async () => {
                this.setState({started: true})
                let mime = 'video/mp4; codecs="avc1.4D4028, mp4a.40.2"';
                videoSourceBuffer = myMediaSource.addSourceBuffer(mime);
                videoSourceBuffer.addEventListener('error', console.error);
                videoSourceBuffer.mode = "sequence";
                let iteration = 0;
                let durationChunk = 4;
                let error = true
                let durationFile;
                while (error) {
                    let {sourceBuffer} = this.state
                    // I think can be optimized with less conversion
                    let outputFileName = file.name;
                    console.log('Copy file', sourceBuffer);
                    await ffmpeg.FS('writeFile', outputFileName, sourceBuffer);
                    durationFile = await getDuration(ffmpeg, outputFileName);
                    try {
                        if (myMediaSource.duration !== durationFile) {
                            myMediaSource.duration = durationFile;
                        }
                    } catch (e) {

                    }
                    console.log('Start transcoding ' + iteration);
                    ffmpeg.setLogger(({type, message}) => {
                        this.log(type + ": " + message);
                        /*
                         * type can be one of following:
                         *
                         * info: internal workflow debug messages
                         * fferr: ffmpeg native stderr output
                         * ffout: ffmpeg native stdout output
                         */
                    });
                    let startDuration = convertHMS(iteration);
                    let chunkDuration = convertHMS(durationChunk);
                    let endDuration = convertHMS(iteration + durationChunk);
                    try {
                        await ffmpeg.run('-i', outputFileName,
                            "-ss", startDuration, "-t", chunkDuration,
                            ...parameter.split(" "),
                            iteration + "_converted.mp4");
                    } catch (e) {
                        await ffmpeg.FS('writeFile', outputFileName, sourceBuffer);
                        durationFile = await getDuration(ffmpeg, outputFileName);
                        myMediaSource.duration = durationFile;
                        error = true;
                        console.error("ERROR: ", e)
                        console.log("Iteration not increased")
                        continue;
                    }
                    console.log("Added piece: " + (startDuration) + " to " + (endDuration) + " on total " + convertHMS(durationFile))
                    let temp = ffmpeg.FS('readFile', iteration + "_converted.mp4");
                    videoSourceBuffer.appendBuffer(temp);
                    this.setState({loading: progress >= 1, progress})
                    iteration += durationChunk
                    if (iteration >= durationFile) {
                        error = false
                        setTimeout(() => {
                            myMediaSource.endOfStream()

                        }, 2000);
                    } else if (iteration > (durationFile - durationChunk)) {
                        durationChunk = durationFile - iteration;
                    }
                }
            });
        } catch (e) {
            console.error("Transcoding error", e)
        }
    }
    initializePlayer = async () => {
        let {ffmpeg} = this.props;
        let {parameter} = this.state;

        this.setState({loading: true, progress: 0})
        console.log('Loading ffmpeg-core.js');
        await load(ffmpeg);
        ffmpeg.setLogger(({type, message}) => {
            this.log(type + ": " + message);
        });
        let myMediaSource = new MediaSource();
        this.videoElement.current.src = URL.createObjectURL(myMediaSource);
        let videoSourceBuffer;
        myMediaSource.addEventListener('sourceopen', async () => {
                console.log("Source open: ")
                this.setState({started: true})
                let mime = 'video/mp4; codecs="avc1.4D4028, mp4a.40.2"';
                videoSourceBuffer = myMediaSource.addSourceBuffer(mime);
                videoSourceBuffer.addEventListener('error', (e) => {
                    console.error("Error: ", e);
                });
                videoSourceBuffer.mode = "sequence";
                let iterableTraduction = async (chunk) => {
                    let {startedPipe} = this.state;
                    console.log("Not enough data: ", chunk.length, videoSourceBuffer.updating)
                    if (chunk.length > 602478 && !startedPipe) {
                        // if (!startedPipe) {
                        this.setState({startedPipe: true})
                        // }
                        console.log("videosource buffer: ", videoSourceBuffer, chunk.length)
                        let sourceBuffer = chunk;
                        let outputFileName = "test.avi";
                        await ffmpeg.FS('writeFile', outputFileName, sourceBuffer);
                        console.log("Duration: ", await getDuration(ffmpeg, outputFileName))
                        myMediaSource.duration = 11;
                        await ffmpeg.run('-i', outputFileName,
                            "-ss", convertHMS(0), "-t", convertHMS(10),
                            ...parameter.split(" "),
                            "asdjhsadhksa.mp4");
                        console.log("Added piece: ", chunk.length)
                        let temp = ffmpeg.FS('readFile', "asdjhsadhksa.mp4");
                        var blob = new Blob([temp], {type: "video/mp4"});
                        var objectUrl = URL.createObjectURL(blob);
                        console.log("TEMP2: ", blob, objectUrl)
                        window.open(objectUrl);
                        console.log("TEMP: ", temp)
                        videoSourceBuffer.appendBuffer(temp);
                    }
                }


            }
        );
    }


    play = async () => {
        // await this.initializePlayer();
        client.add(torrentId, async (torrent) => {
            let {ffmpeg} = this.props;
            await load(ffmpeg);
            // Torrents can contain many files. Let's use the .mp4 file
            let fileTemp = torrent.files.find(function (file) {
                return file.name.endsWith(".avi") || file.name.endsWith(".mkv");
            });
            console.info("file ready", fileTemp);
            fileTemp.on("stream", async (input, cb) => {
                console.info("stream on going", input);
                let {stream, file, req} = input;
                const transcoder = new Transcoder((chunk) => {
                    let {sourceBuffer} = this.state;
                    if (sourceBuffer) {
                        sourceBuffer = new Uint8Array([...sourceBuffer, ...chunk]);
                    } else {
                        sourceBuffer = chunk;
                    }
                    this.setState({sourceBuffer}, () => {
                        let {started} = this.state;
                        if (file.progress > 0.1 && !started) {
                            this.transcode(file);
                        }
                    })
                });
                stream.pipe(transcoder)
            });
            let video = document.getElementById("video");
            fileTemp.streamTo(video)
        });
    }

    render() {
        let {
            loading,
            theme,
            file,
            progress,
            started,
            logs,
            parameter
        } = this.state;
        return (
            <ThemeProvider theme={theme}>
                <CssBaseline/>
                <Container maxWidth="false">
                    <Stack key={"FIRST-ELEMENT"} justifyContent={"center"} spacing={2}>
                        {/*<Stack*/}
                        {/*    key={"SECOND-ELEMENT"}*/}
                        {/*    sx={{*/}
                        {/*        width: "100%",*/}
                        {/*        backgroundColor: "background.paper",*/}
                        {/*        [defaultTheme.breakpoints.down('md')]: {*/}
                        {/*            display: "none",*/}
                        {/*        },*/}
                        {/*        padding: "10px"*/}
                        {/*    }}*/}
                        {/*    spacing={2}*/}
                        {/*    direction={"row"}*/}
                        {/*    alignItems={"center"}*/}
                        {/*    justifyContent={"center"}>*/}
                        {/*    /!*<PlayCircleOutlined sx={{fontSize: 70}} color={"primary"}/>*!/*/}
                        {/*    /!*<Typography variant={"h1"} color={"primary"}>JSTranscoder</Typography>*!/*/}
                        {/*    <Logo/>*/}
                        {/*</Stack>*/}
                        <Stack key={"THIRD-ELEMENT"} alignItems={"center"} spacing={2}>

                            <Typography variant={"h4"}>Select a file to convert</Typography>
                            {file ? <Chip label={file[0].name} variant="outlined"
                                          onDelete={() => {
                                              this.setState({file: null, started: false, logs: []}, () => {
                                                  window.location.reload();
                                              })
                                          }}
                                />
                                :
                                <Button variant="contained"
                                        startIcon={<FileUploadOutlined/>}
                                        component="label"
                                >
                                    Upload
                                    <input
                                        type="file"
                                        hidden
                                        multiple={false}
                                        accept={["video/mp4", "video/x-m4v", "video/*"]}
                                        onChange={(e) => {
                                            this.setState({
                                                file: e.target.files
                                            })
                                        }}
                                    />
                                </Button>
                            }
                            <Button
                                color={"primary"}
                                variant={"contained"}
                                endIcon={<PlayCircleOutlined/>}
                                onClick={this.play}
                            >
                                Play
                            </Button>
                            <Typography variant={"h6"}>Parameters:</Typography>
                            <TextField
                                fullWidth
                                id="parameter"
                                label="Parameters"
                                variant="filled"
                                value={parameter}
                                onChange={(e) => {
                                    let value = e.target.value;
                                    this.setState({
                                        parameter: value
                                    })
                                }}
                            />

                            <Button
                                disabled={file == null}
                                color={"primary"}
                                variant={"contained"}
                                endIcon={<PlayCircleOutlined/>}
                                onClick={this.transcode}
                            >
                                Transcode {loading && progress && "" + progress + "%"}
                            </Button>

                            <Stack sx={{display: started ? undefined : "none", maxWidth: "100%"}}>
                                <video ref={this.videoElement} controls onError={(e) => {
                                    console.error("Error: " + e.target.error.message + " CODE: " + e.target.error.code);
                                }}/>
                            </Stack>

                            <Stack sx={{
                                display: logs.length > 0 ? undefined : "none",
                                maxHeight: "200px",
                                overflow: "auto",
                                maxWidth: "50%",
                                padding: "10px"
                            }}>
                                <Typography variant={"h5"} color={"fine"}>Log of the operation:</Typography>
                                {logs.reverse().map((l, index) => {
                                    return <Typography key={index + "logLine"} variant={"body2"}
                                                       color={"fine"}>{l}</Typography>
                                })}
                            </Stack>
                            <video style={{display: "none"}} controls id="video"/>
                        </Stack>
                    </Stack>
                </Container>
            </ThemeProvider>
        );
    }

}

export default VideoTranscoder;
