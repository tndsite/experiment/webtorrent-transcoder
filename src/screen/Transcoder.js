import * as stream from "stream";

export default class Transcoder extends stream.Transform {


    constructor(ffmpeg) {
        super();
        this.ffmpeg = ffmpeg
    }

    _transform = async (chunk, encoding, next) => {
        console.log("I'm transcoding")
        await this.ffmpeg(chunk)
        next();
    }

}
